<?php
/*
./app/modeles/PostsGestionnaire.php
 */

namespace App\Modeles;
use \Noyau\Classes\App;

class PostsGestionnaire extends \Noyau\Classes\GestionnaireGenerique {

  public function __construct(){
    $this->_table = 'posts';
    $this->_modele = '\App\Modeles\Post';
  }

  public function findAllByCategorie(int $id){
   $sql = "SELECT *
        FROM posts
        JOIN post_has_categories ON post=post.id
        WHERE categorie.id=:id:";

   $rs= App::getConnexion()->prepare($sql);
   $rs->bindValue(':id', $id, \PDO::PARAM_INT);
   return $this->convertPDOStatementToArrayObj($rs);
 }
}
