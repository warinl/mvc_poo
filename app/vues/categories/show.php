<?php
/*
./app/vues/categories/show.php
Variables disponibles :
    - $categorie Categories
 */
?>
<?php
   GLOBAL $content1, $title;
   $title = $categorie->getTitre();
   ob_start();
?>
<h2><?php echo $categorie->getTitre(); ?></h2>
<?php
$ctrl = new \App\Controleur\PostsControleur();
$ctrl->indexByCategorieAction($categorie->getId());
?>

<?php $content1 = ob_get_clean(); ?>
